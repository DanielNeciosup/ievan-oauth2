package net.impacto.oauth2.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.oauth2.config.AppDAOConfigTest;
import net.impacto.oauth2.config.AppServiceConfig;
import net.impacto.oauth2.dtos.request.ObtainClientRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.ClientResponse;
import net.impacto.oauth2.entity.Client;
import net.impacto.oauth2.entity.Error;
import net.impacto.oauth2.enums.ResponseApi;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class, AppServiceConfig.class })
public class ClientServiceTest {
	@Autowired
	private ClientService clientService;
	
	@Test
	public void getClientTest() {
		ObtainClientRequest request = new ObtainClientRequest();
		BaseResponse<ClientResponse> response = new BaseResponse<>();
		
		request.setClientID("ievan_website");
		request.setClientSecret("DfwOrnSthYgkODsh8DQiHw==");
		
		try {
			response = clientService.getClient(request);
			if (response.getState() == ResponseApi.OK.getCode()) {
				Client client = response.getData().getClient();
				System.out.println(client.toString());
			}
			else {
				List<Error> listErrors = response.getListErrors();
				for (Error error : listErrors) {
					System.out.println(error.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}