package net.impacto.oauth2.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.oauth2.config.AppDAOConfigTest;
import net.impacto.oauth2.config.AppServiceConfig;
import net.impacto.oauth2.dtos.request.ObtainUserRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.UserResponse;
import net.impacto.oauth2.entity.Error;
import net.impacto.oauth2.entity.User;
import net.impacto.oauth2.enums.ResponseApi;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class, AppServiceConfig.class })
public class UserServiceTest {
	@Autowired
	private UserService userService;
	
	@Test
	public void getUserByCredentialsTest() {
		ObtainUserRequest request = new ObtainUserRequest();
		request.setUsername("rarriolac");
		request.setPassword("1234567");
		
		try {
			BaseResponse<UserResponse> response = userService.getUserByCredentials(request);
			if (response.getState() == ResponseApi.OK.getCode()) {
				User user = response.getData().getUser();
				System.out.println(user.toString());
			}
			else {
				List<Error> listErrors = response.getListErrors();
				for (Error error : listErrors) {
					System.out.println(error.toString());
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}