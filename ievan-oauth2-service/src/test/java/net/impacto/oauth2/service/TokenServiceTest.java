package net.impacto.oauth2.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.oauth2.config.AppDAOConfigTest;
import net.impacto.oauth2.config.AppServiceConfig;
import net.impacto.oauth2.dtos.request.ObtainTokenRequest;
import net.impacto.oauth2.dtos.response.TokenResponse;
import net.impacto.oauth2.entity.Error;
import net.impacto.oauth2.enums.ResponseApi;
import net.impacto.oauth2.helpers.TokenHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class, AppServiceConfig.class })
public class TokenServiceTest {
	@Autowired
	@Qualifier("tokenService")
	private TokenService tokenService;
	
	@Test
	public void getAccessTokenTest() {
		ObtainTokenRequest request = new ObtainTokenRequest();
		TokenResponse response = new TokenResponse();
		
		request.setAuthorization("Basic aWV2YW5fY21zOlV0Zm03VlMvYlBFL3hySTl6Q0RtRFE9PQ==");
		request.setGrantType("password");
		request.setUsername("jbecerras");
		request.setPassword("123456");
		request.setRefreshToken(TokenHelper.generate());
		
		try {
			response = tokenService.getAccessToken(request);
			
			if (response.getState() == ResponseApi.OK.getCode()) {
				System.out.println(response.toString());				
			}
			else
			{
				List<Error> listErrors = response.getListErrors();
				for (Error error : listErrors) {
					System.out.println(error.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
