package net.impacto.oauth2.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.oauth2.config.AppDAOConfigTest;
import net.impacto.oauth2.config.AppServiceConfig;
import net.impacto.oauth2.utils.HttpBasicAuthenticationHeader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class, AppServiceConfig.class })
public class HttpBasicAuthenticationHeaderTest {
	@Test
	public void decodeTest() {
		String[] clientCredentials = HttpBasicAuthenticationHeader.decode("aWV2YW5fY21zOlV0Zm03VlMvYlBFL3hySTl6Q0RtRFE9PQ==");
		for (String string : clientCredentials) {
			System.out.println(string);
		}
	}
	
	@Test
	public void createEncodedTextTest() {
		String basicAuth = HttpBasicAuthenticationHeader.createEncodedText("ievan_cms", "Utfm7VS/bPE/xrI9zCDmDQ==");
		System.out.println(basicAuth);
	}
}
