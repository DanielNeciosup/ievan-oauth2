package net.impacto.oauth2.helpers;

import java.util.UUID;

public class TokenHelper {
	public static String generate()
	{
		UUID token = UUID.randomUUID();
		return String.valueOf(token);
	}
}
