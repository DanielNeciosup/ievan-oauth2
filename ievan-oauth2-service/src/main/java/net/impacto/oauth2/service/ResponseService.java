package net.impacto.oauth2.service;

import java.util.List;

import net.impacto.oauth2.dtos.request.ObtainPaginationRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.entity.Error;
import net.impacto.oauth2.entity.Pagination;

public interface ResponseService {
	public <T> BaseResponse<T> onSuccess(BaseResponse<T> response, T data, Pagination pagination, String message);

    public <T> BaseResponse<T> onSuccess(BaseResponse<T> response, T data, Pagination pagination);

    public <T> BaseResponse<T> onServerError(Exception e, BaseResponse<T> response, String message);

    public <T> BaseResponse<T> onServerError(Exception e, BaseResponse<T> response);
    
    public <T> BaseResponse<T> onBusinessError(BaseResponse<T> response);
    
    public <T> BaseResponse<T> onBusinessError(BaseResponse<T> response, List<Error> listErrors);

    public <T> BaseResponse<T> addBusinessError(BaseResponse<T> response, String message);
    
	public <T> Pagination calculatePagination(boolean isEmptyList, ObtainPaginationRequest fpgRequest, Integer total, String uriWebService);
	
	public <T> BaseResponse<T> addError(BaseResponse<T> response, Error error);
}
