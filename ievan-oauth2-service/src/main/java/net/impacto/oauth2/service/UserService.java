package net.impacto.oauth2.service;

import net.impacto.oauth2.dtos.request.ObtainUserRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.ListUsersResponse;
import net.impacto.oauth2.dtos.response.UserResponse;

public interface UserService {
	public BaseResponse<ListUsersResponse> listUsers(ObtainUserRequest request) throws Exception;
	
	public BaseResponse<UserResponse> getUserByCredentials(ObtainUserRequest request) throws Exception;
}
