package net.impacto.oauth2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.oauth2.dao.UserDAO;
import net.impacto.oauth2.dtos.request.ObtainUserRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.ListUsersResponse;
import net.impacto.oauth2.dtos.response.UserResponse;
import net.impacto.oauth2.entity.User;
import net.impacto.oauth2.service.UserService;

@Service("userService")
public class UserServiceImpl extends BaseServiceImpl implements UserService {
	@Autowired
	private UserDAO userDAO;
	
	@Override
	public BaseResponse<ListUsersResponse> listUsers(ObtainUserRequest request) throws Exception {
		BaseResponse<ListUsersResponse> response = new BaseResponse<>();
		ListUsersResponse data = new ListUsersResponse();
		
		List<User> listUsers = userDAO.listUsers(request);
		data.setListUsers(listUsers);
		
		return responseService.onSuccess(response, data, null);
	}

	@Override
	public BaseResponse<UserResponse> getUserByCredentials(ObtainUserRequest request) throws Exception {
		BaseResponse<UserResponse> response = new BaseResponse<>();
		UserResponse data = new UserResponse();
		
		List<User> listUsers = userDAO.getUserByCredentials(request);
		User user = listUsers.isEmpty() ? null : listUsers.get(0);
		
		if (user == null) {
			response = responseService.addBusinessError(response, "Usuario o contraseña incorrecto(s)");
		}
		else {
			data.setUser(user);
			response = responseService.onSuccess(response, data, null);
		}
		
		return response;
	}
}
