package net.impacto.oauth2.service;

import net.impacto.oauth2.dtos.request.ObtainClientRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.ClientResponse;
import net.impacto.oauth2.dtos.response.ListClientsResponse;

public interface ClientService {
	 public BaseResponse<ListClientsResponse> listClients(ObtainClientRequest request) throws Exception;
	 
	 public BaseResponse<ClientResponse> getClient(ObtainClientRequest request) throws Exception;
} 
