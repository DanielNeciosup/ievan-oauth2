package net.impacto.oauth2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.oauth2.dao.TokenDAO;
import net.impacto.oauth2.dtos.request.CheckTokenRequest;
import net.impacto.oauth2.dtos.request.ObtainTokenRequest;
import net.impacto.oauth2.dtos.request.ObtainUserRequest;
import net.impacto.oauth2.dtos.request.SendTokenRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.TokenResponse;
import net.impacto.oauth2.entity.Confirmation;
import net.impacto.oauth2.enums.ResponseApi;
import net.impacto.oauth2.service.TokenService;
import net.impacto.oauth2.service.UserService;

@Service("passwordTokenService")
public class PasswordTokenServiceImpl extends BaseServiceImpl implements TokenService {
	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenDAO tokenDAO;
	
	@Override
	public TokenResponse getAccessToken(ObtainTokenRequest request) throws Exception {
		TokenResponse response = new TokenResponse();
		
		var pouRq = new ObtainUserRequest();
		
		pouRq.setUsername(request.getUsername());
		pouRq.setPassword(request.getPassword());
		
		var userRp = userService.getUserByCredentials(pouRq);
		
		if (userRp.getState() == ResponseApi.OK.getCode()) {
			SendTokenRequest stRq = new SendTokenRequest();
			
			stRq.setClientId(request.getClientID());
			stRq.setUsername(request.getUsername());
			stRq.setAccessToken(request.getAccessToken());
			stRq.setRefreshToken(request.getRefreshToken());
			stRq.setGrantType(request.getGrantType());
			
			var tokenResponse = createAccessToken(stRq);
			if (tokenResponse.getState() == ResponseApi.OK.getCode()) {
				response.setAccessToken(stRq.getAccessToken());
				response.setRefreshToken(stRq.getRefreshToken());
				response.setInfo(userRp.getData().getUser());
				
				response = (TokenResponse) responseService.onSuccess(response, null, null);
			}
			else {
				response = (TokenResponse) responseService.onBusinessError(response, tokenResponse.getListErrors());
			}
		}
		else if (userRp.getState() == ResponseApi.BUSINESS_ERROR.getCode()){
			response = (TokenResponse) responseService.onBusinessError(response, userRp.getListErrors());
		}
		
		return response;
	}

	@Override
	public BaseResponse<?> createAccessToken(SendTokenRequest request) throws Exception {
		BaseResponse<?> response = new BaseResponse<>();
		
		List<Confirmation> listConfirmations = tokenDAO.createAccessTokenForUser(request);
		Confirmation confirmation = listConfirmations.isEmpty() ? null : listConfirmations.get(0);
		
		if (confirmation.getId() == ResponseApi.OK.getCode()) {
			response = responseService.onSuccess(response, null, null);
		}
		else if (confirmation.getId() == ResponseApi.BUSINESS_ERROR.getCode()){
			for (Confirmation c : listConfirmations) {
				response = responseService.addBusinessError(response, c.getMessage());
			}
		}
		
		return response;
	}

	@Override
	public BaseResponse<?> checkAccessToken(CheckTokenRequest request) throws Exception {
		return null;
	}
}
