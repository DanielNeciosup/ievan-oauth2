package net.impacto.oauth2.service.impl;

import org.springframework.stereotype.Service;

import net.impacto.oauth2.dtos.request.CheckTokenRequest;
import net.impacto.oauth2.dtos.request.ObtainTokenRequest;
import net.impacto.oauth2.dtos.request.SendTokenRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.TokenResponse;
import net.impacto.oauth2.service.TokenService;

@Service("refreshTokenService")
public class RefreshTokenServiceImpl extends BaseServiceImpl implements TokenService {

	@Override
	public TokenResponse getAccessToken(ObtainTokenRequest request) throws Exception {
		return null;
	}

	@Override
	public BaseResponse<?> createAccessToken(SendTokenRequest request) throws Exception {
		return null;
	}

	@Override
	public BaseResponse<?> checkAccessToken(CheckTokenRequest request) throws Exception {
		return null;
	}
}
