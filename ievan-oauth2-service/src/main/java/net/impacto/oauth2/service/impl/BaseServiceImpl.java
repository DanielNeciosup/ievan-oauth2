package net.impacto.oauth2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.oauth2.service.ResponseService;

@Service("baseService")
public class BaseServiceImpl {
	@Autowired
	protected ResponseService responseService;
}
