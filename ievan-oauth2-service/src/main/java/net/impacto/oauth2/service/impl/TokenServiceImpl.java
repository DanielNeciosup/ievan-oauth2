package net.impacto.oauth2.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.oauth2.dtos.request.CheckTokenRequest;
import net.impacto.oauth2.dtos.request.ObtainClientRequest;
import net.impacto.oauth2.dtos.request.ObtainTokenRequest;
import net.impacto.oauth2.dtos.request.SendTokenRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.TokenResponse;
import net.impacto.oauth2.entity.Client;
import net.impacto.oauth2.enums.AuthGrantTypeEnum;
import net.impacto.oauth2.enums.ResponseApi;
import net.impacto.oauth2.helpers.TokenHelper;
import net.impacto.oauth2.service.ClientService;
import net.impacto.oauth2.service.TokenService;
import net.impacto.oauth2.utils.HttpBasicAuthenticationHeader;

@Service("tokenService")
public class TokenServiceImpl extends BaseServiceImpl implements TokenService {
	private static final String TOKEN_TYPE = "bearer";
	
	@Resource(name = "passwordTokenService")
	private TokenService passwordTokenService;
	
	@Resource(name = "refreshTokenService")
	private TokenService refreshTokenService;
	
	@Autowired
	private ClientService clientService;
	
	@Override
	public TokenResponse getAccessToken(ObtainTokenRequest request) throws Exception {
		TokenResponse response = new TokenResponse();
		
		List<String> listGrants = Stream.of(AuthGrantTypeEnum.values()).map(AuthGrantTypeEnum::getGrantName).collect(Collectors.toList()); 
		
		if (listGrants.contains(request.getGrantType())) {
			if (request.getAuthorization() != null && request.getAuthorization().indexOf("Basic") != -1)
			{
				String authorizationHeaders[] = request.getAuthorization().split(" ");

				String clientID = HttpBasicAuthenticationHeader.decode(authorizationHeaders[1])[0];
				String clientSecret = HttpBasicAuthenticationHeader.decode(authorizationHeaders[1])[1];
				
				var oClRq = new ObtainClientRequest();
				
				oClRq.setClientID(clientID);
				oClRq.setClientSecret(clientSecret);
				
				var clientRp = clientService.getClient(oClRq);
				
				if (clientRp.getState() == ResponseApi.OK.getCode()) {
					request.setClientID(clientID);
					request.setClientSecret(clientSecret);
					request.setAccessToken(TokenHelper.generate());

					if (request.getGrantType().equals(AuthGrantTypeEnum.PASSWORD.getGrantName())) {
						request.setRefreshToken(TokenHelper.generate());
						response = passwordTokenService.getAccessToken(request);
					}
					else if (request.getGrantType().equals(AuthGrantTypeEnum.CLIENT_CREDENTIALS.getGrantName())) {
						request.setUsername(clientID);
						request.setPassword(clientSecret);
						request.setRefreshToken(TokenHelper.generate());
						response = passwordTokenService.getAccessToken(request);
					}
					else if (request.getGrantType().equals(AuthGrantTypeEnum.REFRESH_TOKEN.getGrantName())) {
						request.setRefreshToken(TokenHelper.generate());
						response = refreshTokenService.getAccessToken(request);
					}
					else if (request.getGrantType().equals(AuthGrantTypeEnum.IMPLICIT.getGrantName())) {
					}
					else if (request.getGrantType().equals(AuthGrantTypeEnum.AUTHORIZATION_CODE.getGrantName())) {
						request.setRefreshToken(TokenHelper.generate());
					}
					
					if (response.getState() == ResponseApi.OK.getCode()) {
						Client client = clientRp.getData().getClient();
						response.setExpiresIn(client.getAccessTokenValidity());
						response.setScope(client.getScope());
						response.setTokenType(TOKEN_TYPE);
						
						response = (TokenResponse) responseService.onSuccess(response, null, null, "Access token generado exitosamente");
					}
				}
				else{
					response = (TokenResponse) responseService.onBusinessError(response, clientRp.getListErrors());
				}
			}
			else
			{
				response = (TokenResponse) responseService.addBusinessError(response, "Parametro de autorizacion erróneo. Asegúrese de que comience con 'Basic' y que la cadena de ");
			}
		}
		else {
			response = (TokenResponse) responseService.addBusinessError(response, "Tipo de autorización no existente en OAuth 2.0");
		}
				
		return response;
	}

	@Override
	public BaseResponse<?> createAccessToken(SendTokenRequest request) throws Exception {
		return null;
	}

	@Override
	public BaseResponse<?> checkAccessToken(CheckTokenRequest request) throws Exception {
		return null;
	}
}
