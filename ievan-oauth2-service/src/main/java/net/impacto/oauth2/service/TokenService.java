package net.impacto.oauth2.service;

import net.impacto.oauth2.dtos.request.CheckTokenRequest;
import net.impacto.oauth2.dtos.request.ObtainTokenRequest;
import net.impacto.oauth2.dtos.request.SendTokenRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.TokenResponse;

public interface TokenService {
	public TokenResponse getAccessToken(ObtainTokenRequest request) throws Exception;
	
	public BaseResponse<?> createAccessToken(SendTokenRequest request) throws Exception;
	
	public BaseResponse<?> checkAccessToken(CheckTokenRequest request) throws Exception;
}
