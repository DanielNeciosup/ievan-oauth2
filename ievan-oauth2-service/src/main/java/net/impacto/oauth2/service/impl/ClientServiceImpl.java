package net.impacto.oauth2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.impacto.oauth2.dao.ClientDAO;
import net.impacto.oauth2.dtos.request.ObtainClientRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.ClientResponse;
import net.impacto.oauth2.dtos.response.ListClientsResponse;
import net.impacto.oauth2.entity.Client;
import net.impacto.oauth2.service.ClientService;

@Service("clientService")
public class ClientServiceImpl extends BaseServiceImpl implements ClientService {
	@Autowired
	private ClientDAO clientDAO;
	
	@Override
	public BaseResponse<ListClientsResponse> listClients(ObtainClientRequest request) throws Exception {
		BaseResponse<ListClientsResponse> response = new BaseResponse<>();
		ListClientsResponse data = new ListClientsResponse();
		
		List<Client> listClients = clientDAO.listClients(request);
		data.setListClients(listClients);
		response = responseService.onSuccess(response, data, null);
		
		return response;
	}

	@Override
	public BaseResponse<ClientResponse> getClient(ObtainClientRequest request) throws Exception {
		BaseResponse<ClientResponse> response = new BaseResponse<>();
		ClientResponse data = new ClientResponse();
		
		List<Client> listClientsRp = listClients(request).getData().getListClients();
		Client client = listClientsRp.isEmpty() ? null : listClientsRp.get(0);
		
		if (client == null) {
			response = responseService.addBusinessError(response, "Aplicación cliente no encontrada");
		}
		else {
			data.setClient(client);
			response = responseService.onSuccess(response, data, null);
		}		
		
		return response;
	}
}
