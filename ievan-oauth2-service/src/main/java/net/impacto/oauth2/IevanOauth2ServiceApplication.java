package net.impacto.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IevanOauth2ServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(IevanOauth2ServiceApplication.class, args);
	}
}
