package net.impacto.oauth2.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = {
	"net.impacto.oauth2.service",
	"net.impacto.oauth2.params"
})
@PropertySource(value = {"classpath:net/impacto/oauth2/config/api.properties"}, ignoreResourceNotFound = true)
public class AppServiceConfig {

}
