package net.impacto.oauth2.infra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IevanOauth2InfraApplication {

	public static void main(String[] args) {
		SpringApplication.run(IevanOauth2InfraApplication.class, args);
	}
}
