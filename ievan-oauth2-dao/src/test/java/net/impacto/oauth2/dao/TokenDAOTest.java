package net.impacto.oauth2.dao;

import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.oauth2.config.AppDAOConfigTest;
import net.impacto.oauth2.dtos.request.SendTokenRequest;
import net.impacto.oauth2.entity.Confirmation;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class })
public class TokenDAOTest {
	@Autowired
	private TokenDAO tokenDAO;
	
	@Test
	public void createAccessTokenForUser() {
		SendTokenRequest request = new SendTokenRequest();
		request.setGrantType("password");
		request.setUsername("taguadot");
		request.setClientId("ievan_cms");
		request.setAccessToken(UUID.randomUUID().toString());
		request.setRefreshToken(UUID.randomUUID().toString());
		
		try {
			List<Confirmation> listConfirmations = tokenDAO.createAccessTokenForUser(request);
			for (Confirmation confirmation : listConfirmations) {
				System.out.println(confirmation.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
