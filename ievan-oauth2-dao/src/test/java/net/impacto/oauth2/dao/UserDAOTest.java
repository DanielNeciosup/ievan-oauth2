package net.impacto.oauth2.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.oauth2.config.AppDAOConfigTest;
import net.impacto.oauth2.dtos.request.ObtainUserRequest;
import net.impacto.oauth2.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class })
public class UserDAOTest {
	@Autowired
	private UserDAO userDAO;
	
	@Test
	public void getUserByCredentialsTest() 
	{
		ObtainUserRequest request = new ObtainUserRequest();
		request.setUsername("jbecerras");
		request.setPassword("123456");
		
		try {
			List<User> listUsers = userDAO.getUserByCredentials(request);
			for (User user : listUsers) {
				System.out.println(user.toString());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
