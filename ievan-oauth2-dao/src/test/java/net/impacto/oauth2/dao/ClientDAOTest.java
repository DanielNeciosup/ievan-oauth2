package net.impacto.oauth2.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.impacto.oauth2.config.AppDAOConfigTest;
import net.impacto.oauth2.dtos.request.ObtainClientRequest;
import net.impacto.oauth2.entity.Client;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppDAOConfigTest.class })
public class ClientDAOTest {
	@Autowired
	private ClientDAO clientDAO;
	
	@Test
	public void listClientsTest()
	{
		ObtainClientRequest request = new ObtainClientRequest();
		request.setClientID("ievan_cms");
		request.setClientSecret("Utfm7VS/bPE/xrI9zCDmDQ==");
		
		try {
			List<Client> listClients = clientDAO.listClients(request);
			for (Client client : listClients) {
				System.out.println(client.toString());				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
