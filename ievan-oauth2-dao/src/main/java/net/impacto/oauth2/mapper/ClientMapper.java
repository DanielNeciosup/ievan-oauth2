package net.impacto.oauth2.mapper;

import java.util.List;
import java.util.Map;

import net.impacto.oauth2.entity.Client;

public interface ClientMapper {
	public List<Client> listClients(Map<String, Object> params);
}