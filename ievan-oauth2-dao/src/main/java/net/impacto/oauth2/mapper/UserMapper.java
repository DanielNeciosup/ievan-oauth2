package net.impacto.oauth2.mapper;

import java.util.List;
import java.util.Map;

import net.impacto.oauth2.entity.User;

public interface UserMapper {
	public List<User> getUserByCredentials(Map<String, Object> params);
	
	public List<User> listUsers(Map<String, Object> params);
}