package net.impacto.oauth2.mapper;

import java.util.List;
import java.util.Map;

import net.impacto.oauth2.entity.Confirmation;

public interface TokenMapper {
	public List<Confirmation> insertAccessTokenPassword(Map<String, Object> params);
}
