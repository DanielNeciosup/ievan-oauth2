package net.impacto.oauth2.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.oauth2.dao.UserDAO;
import net.impacto.oauth2.dtos.request.ObtainUserRequest;
import net.impacto.oauth2.entity.User;
import net.impacto.oauth2.mapper.UserMapper;

@Repository("userDAO")
@Transactional
public class UserDAOImpl implements UserDAO{
	@Autowired
	private UserMapper userMapper;
	
	@Override
	public List<User> listUsers(ObtainUserRequest request) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		params.put("id", request.getId());
		params.put("username", request.getUsername());
		params.put("firstName", request.getFirstName());
		params.put("lastName", request.getLastName());
		params.put("email", request.getEmail());
		
		return userMapper.listUsers(params);
	}

	@Override
	public List<User> getUserByCredentials(ObtainUserRequest request) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		params.put("username", request.getUsername());
		params.put("password", request.getPassword());
		
		return userMapper.getUserByCredentials(params);
	}
}