package net.impacto.oauth2.dao;

import java.util.List;

import net.impacto.oauth2.dtos.request.ObtainUserRequest;
import net.impacto.oauth2.entity.User;

public interface UserDAO {
	public List<User> getUserByCredentials(ObtainUserRequest request) throws Exception;
	
	public List<User> listUsers(ObtainUserRequest request) throws Exception;
}
