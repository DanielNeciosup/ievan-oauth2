package net.impacto.oauth2.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.oauth2.dao.TokenDAO;
import net.impacto.oauth2.dtos.request.SendTokenRequest;
import net.impacto.oauth2.entity.Confirmation;
import net.impacto.oauth2.mapper.TokenMapper;

@Repository("tokenDAO")
@Transactional
public class TokenDAOImpl implements TokenDAO{
	@Autowired
	private TokenMapper tokenMapper;
	
	@Override
	public List<Confirmation> createAccessTokenForUser(SendTokenRequest request) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("grantType", request.getGrantType());
		params.put("username", request.getUsername());
		params.put("clientId", request.getClientId());
		params.put("accessToken", request.getAccessToken());
		params.put("refreshToken", request.getRefreshToken());
		
		return tokenMapper.insertAccessTokenPassword(params);
	}
}
