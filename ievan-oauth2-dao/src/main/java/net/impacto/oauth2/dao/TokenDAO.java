package net.impacto.oauth2.dao;

import net.impacto.oauth2.dtos.request.SendTokenRequest;
import net.impacto.oauth2.entity.Confirmation;

import java.util.List;

public interface TokenDAO {
	public List<Confirmation> createAccessTokenForUser(SendTokenRequest request) throws Exception;
}
