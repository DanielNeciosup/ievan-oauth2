package net.impacto.oauth2.dao;

import java.util.List;

import net.impacto.oauth2.dtos.request.ObtainClientRequest;
import net.impacto.oauth2.entity.Client;

public interface ClientDAO {
	public List<Client> listClients(ObtainClientRequest request) throws Exception;
}
