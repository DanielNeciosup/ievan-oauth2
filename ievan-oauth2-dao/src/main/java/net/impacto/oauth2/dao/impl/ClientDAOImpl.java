package net.impacto.oauth2.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.impacto.oauth2.dao.ClientDAO;
import net.impacto.oauth2.dtos.request.ObtainClientRequest;
import net.impacto.oauth2.entity.Client;
import net.impacto.oauth2.mapper.ClientMapper;

@Repository("clientDAO")
@Transactional
public class ClientDAOImpl implements ClientDAO {
	@Autowired
	private ClientMapper clientMapper;
	
	@Override
	public List<Client> listClients(ObtainClientRequest request) throws Exception {
		Map<String, Object> params = new HashMap<>();
		
		params.put("clientId", request.getClientID());
		params.put("clientSecret", request.getClientSecret());
		
		return clientMapper.listClients(params);
	}
}
