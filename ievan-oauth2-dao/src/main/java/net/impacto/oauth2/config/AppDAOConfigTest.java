package net.impacto.oauth2.config;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {
	"net.impacto.oauth2.dao",
	"net.impacto.oauth2.mapper"
})  
@MapperScan("net.impacto.oauth2.mapper")
@PropertySource(value = "classpath:/net/impacto/oauth2/config/log4j2.properties", ignoreResourceNotFound = true)
@PropertySource(value = "classpath:/net/impacto/oauth2/config/jdbc.properties", ignoreResourceNotFound = true)
public class AppDAOConfigTest {
	@Value("${jdbc.username}")
	private String username;
	
	@Value("${jdbc.password}")
	private String password;
	
	@Value("${jdbc.url}")
	private String url;
	
	@Value("${jdbc.driverClassName}")
	private String driverClassName;
		
	@Bean
	public DataSource getDSIevanAuthByCredentials() {
		return DataSourceBuilder.create()
			   .username(username)
			   .password(password)
			   .url(url)
			   .driverClassName(driverClassName)
			   .build();
	}
	
	@Bean
	public DataSourceTransactionManager transactionManager() throws Exception 
	{
		return new DataSourceTransactionManager(getDSIevanAuthByCredentials());
	}

	@Bean
	public SqlSessionFactoryBean sqlSessionFactory() throws Exception 
	{
		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();

		sessionFactory.setDataSource(getDSIevanAuthByCredentials());

		return sessionFactory;
	}
}
