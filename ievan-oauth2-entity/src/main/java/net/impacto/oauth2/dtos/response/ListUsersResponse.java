package net.impacto.oauth2.dtos.response;

import java.util.List;

import net.impacto.oauth2.entity.User;

public class ListUsersResponse {
	private List<User> listUsers;

	public List<User> getListUsers() {
		return listUsers;
	}

	public void setListUsers(List<User> listUsers) {
		this.listUsers = listUsers;
	}
}
