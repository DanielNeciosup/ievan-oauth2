package net.impacto.oauth2.dtos.response;

import net.impacto.oauth2.entity.User;

public class TokenResponse extends BaseResponse<String> {
	private String accessToken;
	private String tokenType;
	private Integer expiresIn;
	private String refreshToken;
	private String scope;
	private User info;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public Integer getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public User getInfo() {
		return info;
	}

	public void setInfo(User info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return "TokenResponse [accessToken=" + accessToken + ", tokenType=" + tokenType + ", expiresIn=" + expiresIn
				+ ", refreshToken=" + refreshToken + ", scope=" + scope + ", info=" + info.toString() + "]";
	}
}
