package net.impacto.oauth2.dtos.response;

import java.util.List;

import net.impacto.oauth2.entity.Client;

public class ListClientsResponse {
	private List<Client> listClients;

	public List<Client> getListClients() {
		return listClients;
	}

	public void setListClients(List<Client> listClients) {
		this.listClients = listClients;
	}
}
