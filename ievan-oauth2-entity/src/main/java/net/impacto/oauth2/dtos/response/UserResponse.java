package net.impacto.oauth2.dtos.response;

import net.impacto.oauth2.entity.User;

public class UserResponse {
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
