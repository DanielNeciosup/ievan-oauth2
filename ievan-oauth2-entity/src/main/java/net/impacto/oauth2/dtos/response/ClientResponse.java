package net.impacto.oauth2.dtos.response;

import net.impacto.oauth2.entity.Client;

public class ClientResponse {
	private Client client;

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
}
