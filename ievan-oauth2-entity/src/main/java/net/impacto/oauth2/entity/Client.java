package net.impacto.oauth2.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String clientID;
	private String clientSecret;
	private String resourcesID;
	private String scope;
	private String authorizedGrantTypes;
	private String redirectURL;
	private String authorities;
	private Integer accessTokenValidity;
	private Integer refreshTokenValidity;
	private String aditionalInformation;
	private String autoApprove;

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getResourcesID() {
		return resourcesID;
	}

	public void setResourcesID(String resourcesID) {
		this.resourcesID = resourcesID;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getAuthorizedGrantTypes() {
		return authorizedGrantTypes;
	}

	public void setAuthorizedGrantTypes(String authorizedGrantTypes) {
		this.authorizedGrantTypes = authorizedGrantTypes;
	}

	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	public String getAuthorities() {
		return authorities;
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	public Integer getAccessTokenValidity() {
		return accessTokenValidity;
	}

	public void setAccessTokenValidity(Integer accessTokenValidity) {
		this.accessTokenValidity = accessTokenValidity;
	}

	public Integer getRefreshTokenValidity() {
		return refreshTokenValidity;
	}

	public void setRefreshTokenValidity(Integer refreshTokenValidity) {
		this.refreshTokenValidity = refreshTokenValidity;
	}

	public String getAditionalInformation() {
		return aditionalInformation;
	}

	public void setAditionalInformation(String aditionalInformation) {
		this.aditionalInformation = aditionalInformation;
	}

	public String getAutoApprove() {
		return autoApprove;
	}

	public void setAutoApprove(String autoApprove) {
		this.autoApprove = autoApprove;
	}

	@Override
	public String toString() {
		return "Client [clientID=" + clientID + ", clientSecret=" + clientSecret + ", resourcesID=" + resourcesID
				+ ", scope=" + scope + ", authorizedGrantTypes=" + authorizedGrantTypes + ", redirectURL=" + redirectURL
				+ ", authorities=" + authorities + ", accessTokenValidity=" + accessTokenValidity
				+ ", refreshTokenValidity=" + refreshTokenValidity + ", aditionalInformation=" + aditionalInformation
				+ ", autoApprove=" + autoApprove + "]";
	}
}
