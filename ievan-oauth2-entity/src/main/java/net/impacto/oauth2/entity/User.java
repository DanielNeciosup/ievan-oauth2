package net.impacto.oauth2.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class User implements Serializable {
	private static final long serialVersionUID = 7415488085584200510L;

	private Integer id;
	private String username;
	private String firstName;
	private String lastName;
	private String email;
	private Boolean passReset;
	private String createdAt;
	private String lastAccessAt;
	private List<Role> listRoles;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getPassReset() {
		return passReset;
	}

	public void setPassReset(Boolean passReset) {
		this.passReset = passReset;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getLastAccessAt() {
		return lastAccessAt;
	}

	public void setLastAccessAt(String lastAccessAt) {
		this.lastAccessAt = lastAccessAt;
	}

	public List<Role> getListRoles() {
		return listRoles;
	}

	public void setListRoles(List<Role> listRoles) {
		this.listRoles = listRoles;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", passReset=" + passReset + ", createdAt=" + createdAt + ", lastAccessAt="
				+ lastAccessAt + ", listRoles=" + listRoles + "]";
	}
}
