package net.impacto.oauth2.entity;

import java.io.Serializable;

public class Confirmation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String message;
	private Integer dataId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getDataId() {
		return dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	@Override
	public String toString() {
		return "Confirmation [id=" + id + ", message=" + message + ", dataId=" + dataId + "]";
	}
}
