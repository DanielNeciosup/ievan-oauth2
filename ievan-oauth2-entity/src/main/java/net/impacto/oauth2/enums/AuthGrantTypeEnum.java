package net.impacto.oauth2.enums;

public enum AuthGrantTypeEnum {
	AUTHORIZATION_CODE("authorization_code"), 
	IMPLICIT("implicit"), 
	PASSWORD("password"), 
	CLIENT_CREDENTIALS("client_credentials"),
	REFRESH_TOKEN("refresh_token");

	private AuthGrantTypeEnum(String grantName) {
		this.grantName = grantName;
	}

	private String grantName;

	public String getGrantName() {
		return grantName;
	}
}
