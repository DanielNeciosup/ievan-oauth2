package net.impacto.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IevanOauth2WsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IevanOauth2WsApplication.class, args);
	}
}
