package net.impacto.oauth2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.impacto.oauth2.service.ResponseService;

@Component
public class BaseController {
	@Autowired
    protected ResponseService responseService;
}
