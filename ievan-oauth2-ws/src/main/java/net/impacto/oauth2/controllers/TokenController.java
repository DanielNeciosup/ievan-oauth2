package net.impacto.oauth2.controllers;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.impacto.oauth2.dtos.request.ObtainTokenRequest;
import net.impacto.oauth2.dtos.response.BaseResponse;
import net.impacto.oauth2.dtos.response.TokenResponse;
import net.impacto.oauth2.service.TokenService;

@RequestMapping("/oauth")
@RestController
public class TokenController extends BaseController{
	@Resource(name = "tokenService")
	private TokenService tokenService;
	
	@RequestMapping(value = "/token", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public TokenResponse getAccessToken(@RequestHeader(value = "Authorization") String authorization,
									    @RequestParam(value = "grant_type", required = true) String grantType, 
									    @RequestParam(value = "username", required = false) String username, 
									    @RequestParam(value = "password", required = false) String password,
									    @RequestParam(value = "refresh_token", required = false) String refreshToken)
	{
		var request = new ObtainTokenRequest();
		TokenResponse response = new TokenResponse();
		
		request.setAuthorization(authorization);
		request.setGrantType(grantType);
		request.setUsername(username);
		request.setPassword(password);
		request.setRefreshToken(refreshToken);
		
		try {
			response = tokenService.getAccessToken(request);
		} 
		catch (Exception e) {
			response = (TokenResponse) responseService.onServerError(e, response);
		}
		
		return response;
	}
	
	@RequestMapping(value = "/checkToken", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public BaseResponse<?> checkToken(@RequestHeader(value = "username") String username,
									  @RequestParam(value = "token", required = true) String token){
		return null;
	}
}
